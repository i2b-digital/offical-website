// import Logo from "@/assets/images/Logo_I2B_BLACK.png";
import useScroll from "@/hook/useScroll";
import { cn } from "@/util/cn";
import { useRef } from "react";
import { HashLink as Link } from "react-router-hash-link";
import Logo from "./Logo";
import NavItem from "./NavItem";

const Header = (props) => {
  const { logo, navs } = props;

  const headerRef = useRef();

  const isScrolled = useScroll();

  return (
    <header
      ref={headerRef}
      className={cn(
        "fixed top-0 z-10 w-full flex justify-between px-2 text-sm transition-all duration-300 md:text-base md:px-6",
        isScrolled ? "bg-white backdrop-blur-md shadow-sm" : " bg-transparent"
      )}
    >
      <div className="py-2">
        <Logo url={logo} className="max-w-[70px] md:w-full md:max-w-[90px]" />
      </div>

      <ul className="flex">
        {navs?.map((nav, index) =>
          nav ? (
            <NavItem
              key={index}
              className={isScrolled ? "text-[#161C24]" : "text-white"}
            >
              <Link to={`#section_${index}`}>{nav}</Link>
            </NavItem>
          ) : null
        )}
      </ul>
    </header>
  );
};

export default Header;
