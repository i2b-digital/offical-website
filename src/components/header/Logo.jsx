const Logo = (props) => {
  const { url, className } = props;

  return <a href="/">{url ? <img src={url} className={className} /> : null}</a>;
};

export default Logo;
