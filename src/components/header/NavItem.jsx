import { cn } from "@/util/cn";

const NavItem = (props) => {
  const { children, className, ...rest } = props;

  return (
    <li
      className={cn(
        "font-light	inline-block px-2 py-4 lg:px-4 lg:py-4",
        className
      )}
      {...rest}
    >
      {children}
    </li>
  );
};

export default NavItem;
