import { FaSquareFacebook, FaInstagram } from "react-icons/fa6";

const Footer = (props) => {
  const { ig, fb } = props;

  return (
    <div className="py-12 lg:mt-20 lg:py-20">
      <div className="flex flex-col items-center">
        <ul className="flex gap-1 mb-4">
          {ig ? (
            <li>
              <a href={ig} target="_blank" rel="noopener noreferrer">
                <FaInstagram size={22} className="text-gray-700" />
              </a>
            </li>
          ) : null}
          {fb ? (
            <li>
              <a href={fb} target="_blank" rel="noopener noreferrer">
                <FaSquareFacebook size={22} className="text-gray-700" />
              </a>
            </li>
          ) : null}
        </ul>

        <span className="text-xs text-slate-500 font-normal">
          © All rights reserved
        </span>
        <span className="text-xs text-slate-500 font-normal">
          made by 聚智數位
        </span>
      </div>
    </div>
  );
};

export default Footer;
