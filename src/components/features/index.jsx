import background from "@/assets/images/bg.jpeg";

const Features = (props) => {
  const { data, title } = props;

  return (
    <div className="overflow-hidden relative">
      <img
        src={background}
        className="hidden md:block absolute -z-[1] opacity-60 -right-[55%] top-[0px]"
      />

      <div id="section_2" className="py-20 md:py-40 max-w-7xl mx-auto">
        <h1 className="text-[#212B36] text-2xl font-black text-center md:mb-20 md:text-4xl">
          {title}
        </h1>

        <ul className="flex flex-col lg:gap-10 lg:flex-row">
          {data.length
            ? data.map((feature, index) => (
                <li
                  key={index}
                  className={`flex-1 m-8 p-8 rounded-lg border text-center md:border-none md:p-16 ${
                    index === 1
                      ? "md:shadow-[-40px_40px_80px_rgba(145,158,171,0.16)]"
                      : ""
                  }`}
                >
                  <div className="flex justify-center">
                    <img
                      src={feature.icon}
                      className="max-w-28 max-h-28 object-cover"
                    />
                  </div>
                  <h2 className="text-[#212B36] text-xl font-bold my-14">
                    {feature.title}
                  </h2>
                  <p className="text-[#212B36] mt-4 text-sm lg:text-base">
                    {feature.description}
                  </p>
                </li>
              ))
            : null}
        </ul>
      </div>
    </div>
  );
};

export default Features;
