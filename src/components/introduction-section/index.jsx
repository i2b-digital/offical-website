import useMedia from "@/hook/useMedia";
import { cn } from "@/util/cn";
import { useEffect, useState } from "react";

const IntroductionSection = (props) => {
  const {
    index,
    imageUrl,
    title,
    description,
    isReverse = false,
    bgClassName = "",
  } = props;

  const { isMd } = useMedia();

  const [direction, setDirection] = useState("");

  useEffect(() => {
    if (!isMd && !isReverse) {
      setDirection("flex-col");
    } else if (!isMd && isReverse) {
      setDirection("flex-col-reverse");
    } else if (isMd && !isReverse) {
      setDirection("flex-row");
    } else {
      setDirection("flex-row-reverse");
    }
  }, [isMd]);

  return (
    <div
      id={`section_${index}`}
      className={cn("flex mx-auto mt-20 md:mt-40", bgClassName)}
    >
      <article
        className={cn(
          "flex items-center px-8 max-w-[1280px] mx-auto md:grid-cols-2 md:gap-12 lg:px-0",
          direction
        )}
      >
        {imageUrl ? (
          <div className={cn("basis-1/2 lg:px-12", isReverse ? "mt-4" : "")}>
            <img src={imageUrl} alt="" />
          </div>
        ) : null}
        <aside
          className={cn("basis-1/2 md:mt-0 md:px-12", isReverse ? "" : "mt-4")}
        >
          <h1 className="text-xl font-bold text-[#212B36]">{title}</h1>
          <p className="mt-6 text-[#212B36]">{description}</p>
        </aside>
      </article>
    </div>
  );
};

export default IntroductionSection;
