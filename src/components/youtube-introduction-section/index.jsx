const YoutubeIndroductionSection = (props) => {
  const { title, data, image } = props;

  return (
    <div id="section_2" className="max-w-7xl mx-auto">
      {title ? (
        <h1 className="p-8 text-[#F0F0F0] text-4xl font-bold whitespace-pre-line md:text-6xl">
          {title}
        </h1>
      ) : null}

      {image ? (
        <div className="px-8 overflow-hidden">
          <img className="w-full h-full max-h-[550px]" src={image} />
        </div>
      ) : null}

      <ul className="flex flex-col gap-6 p-8 md:flex-row">
        {data.length
          ? data.map((url, i) => (
              <li key={i} className="flex-1">
                {/* 只能放嵌入式網址 */}
                <iframe
                  className="w-full h-auto aspect-video"
                  src={url}
                  frameborder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowfullscreen
                ></iframe>
              </li>
            ))
          : null}
      </ul>
    </div>
  );
};

export default YoutubeIndroductionSection;
