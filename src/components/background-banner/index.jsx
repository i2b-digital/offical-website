import { cn } from "@/util/cn";

const BackgroundBanner = (props) => {
  const { imageUrl, title, url, className } = props;

  return imageUrl ? (
    <div
      className={cn(
        "flex flex-col gap-6 items-center justify-center h-[22rem] md:h-[30rem] bg-center bg-cover bg-fixed bg-no-repeat",
        className
      )}
      style={{ backgroundImage: `url(${imageUrl})` }}
    >
      <p
        className={cn(
          "text-center text-white font-black w-1/2 text-4xl md:text-6xl",
          url ? "md:text-4xl" : "md:text-6xl"
        )}
      >
        {title}
      </p>

      {url ? (
        <a href={url} className="px-8 py-4 bg-white font-light rounded">
          前往探索
        </a>
      ) : null}
    </div>
  ) : null;
};

export default BackgroundBanner;
