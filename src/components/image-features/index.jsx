import { cn } from "@/util/cn";

const ImageFeatures = (props) => {
  const { title, data } = props;

  const hasData = data?.every(
    (item) =>
      item.icon !== undefined &&
      item.title !== undefined &&
      item.description !== undefined
  );

  if (!hasData) return null;

  return (
    <div id="section_1" className="py-10 md:py-20 max-w-7xl mx-auto">
      {title ? (
        <h1 className="text-[#F0F0F0] text-4xl font-bold whitespace-pre-line p-8 md:text-6xl">
          {title}
        </h1>
      ) : null}

      <ul className="flex flex-col gap-6 lg:gap-10 lg:flex-row lg:justify-center">
        {data.length
          ? data.map((feature, index) => (
              <li
                key={index}
                className={cn(
                  "m-8 p-8 text-center relative  flex flex-col items-center justify-center md:p-16",
                  feature?.url ? "cursor-pointer" : ""
                )}
              >
                <a href={feature?.url || "#"} target="_blank">
                  {feature.icon ? (
                    <img
                      src={feature.icon}
                      className="max-h-[550px] absolute inset-0 w-full h-full object-cover z-[-1]"
                    />
                  ) : null}

                  {feature.title ? (
                    <h2 className="text-white text-2xl font-bold my-6 border border-white px-4 py-2">
                      {feature.title}
                    </h2>
                  ) : null}

                  {feature.description ? (
                    <p className="text-white mt-4 text-sm lg:text-base">
                      {feature.description}
                    </p>
                  ) : null}
                </a>
              </li>
            ))
          : null}
      </ul>
    </div>
  );
};

export default ImageFeatures;
