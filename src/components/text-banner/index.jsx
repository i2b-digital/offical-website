import { cn } from "@/util/cn";

const TextBanner = (props) => {
  const { title, description, bgColor, fontColor } = props;

  return (
    <div
      className={cn(
        "px-10 py-28 md:py-[8rem]",
        bgColor ? bgColor : "bg-[#161C24]"
      )}
    >
      <div className="flex items-center justify-center gap-4 mb-4">
        <p className={cn("text-2xl", fontColor ? fontColor : "text-white")}>
          {title}
        </p>
      </div>
      <p
        className={cn(
          "font-thin text-center px-6",
          fontColor ? fontColor : "text-white"
        )}
      >
        {description}
      </p>
    </div>
  );
};

export default TextBanner;
