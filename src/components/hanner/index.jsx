import { cn } from "@/util/cn";
import { useEffect, useRef } from "react";

const Banner = (props) => {
  const { title, videoUrl, imageUrl, className } = props;

  const videoRef = useRef();

  useEffect(() => {
    videoRef.current?.load();
  }, [videoUrl]);

  return (
    <div
      className={cn("relative h-full md:h-[68vh] overflow-hidden", className)}
    >
      <p className="w-full text-center absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-white font-bold tracking-widest text-xl md:text-4xl lg:text-5xl">
        {title}
      </p>
      {videoUrl ? (
        <video
          ref={videoRef}
          autoPlay
          muted
          loop
          className="h-full w-full object-cover"
        >
          <source src={videoUrl} type="video/mp4" />
        </video>
      ) : (
        <img className="object-cover w-full h-full" src={imageUrl} />
      )}
    </div>
  );
};

export default Banner;
