import { cn } from "@/util/cn";
import {
  LiaPhoneSolid,
  LiaFaxSolid,
  LiaMailBulkSolid,
  LiaMapMarkerAltSolid,
} from "react-icons/lia";

const ContactSection = (props) => {
  const { image, phone, fax, email, address } = props;

  if (!phone && !fax && !email && !address) {
    return null;
  }

  return (
    <div id="section_3" className="pb-10 border-b md:py-20 max-w-7xl mx-auto">
      <h1 className="text-[#F0F0F0] text-4xl font-bold whitespace-pre-line px-8 md:text-6xl">
        聯絡我們
      </h1>

      <section className="flex flex-col lg:items-start lg:flex-row lg:justify-between">
        <ul className="mt-10 px-8 grid gap-4 md:gap-0">
          <li
            className={cn(
              "border p-3 rounded md:border-none",
              phone ? "" : "hidden"
            )}
          >
            <div className="flex items-center gap-2">
              <LiaPhoneSolid size={22} />
              <label className="font-bold text-[#212B36]">電話</label>
            </div>
            <p className="pl-[30px] text-[#212B36]">{phone}</p>
          </li>

          <li
            className={cn(
              "border p-3 rounded md:border-none",
              fax ? "" : "hidden"
            )}
          >
            <div className="flex items-center gap-2">
              <LiaFaxSolid size={22} />
              <label className="font-bold text-[#212B36]">傳真</label>
            </div>
            <p className="pl-[30px] text-[#212B36]">{fax}</p>
          </li>

          <li
            className={cn(
              "border p-3 rounded md:border-none",
              email ? "" : "hidden"
            )}
          >
            <div className="flex items-center gap-2">
              <LiaMailBulkSolid size={22} />
              <label className="font-bold text-[#212B36]">E mail</label>
            </div>
            <p className="pl-[30px] text-[#212B36]">{email}</p>
          </li>

          <li
            className={cn(
              "border p-3 rounded md:border-none",
              address ? "" : "hidden"
            )}
          >
            <div className="flex items-center gap-2">
              <LiaMapMarkerAltSolid size={22} />
              <label className="font-bold text-[#212B36]">地址</label>
            </div>
            <p className="pl-[30px] text-[#212B36]">{address}</p>
          </li>
        </ul>

        <div className={cn("p-8 overflow-hidden", image ? "" : "hidden")}>
          <img
            className="w-full lg:max-w-[550px] lg:max-h-[600px]"
            src={image}
          />
        </div>
      </section>
    </div>
  );
};

export default ContactSection;
