import { useMediaQuery } from "react-responsive";

const useMedia = () => {
  const isMd = useMediaQuery({ query: "(min-width: 768px)" });
  const isLg = useMediaQuery({ query: "(min-width: 1024px)" });
  const isXl = useMediaQuery({ query: "(min-width: 1028px)" });

  return { isMd, isLg, isXl };
};

export default useMedia;
