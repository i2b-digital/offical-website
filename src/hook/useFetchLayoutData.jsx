import useSWR from "swr";
import { getSubdomain } from "@/util/getSubdomain";

const LAYOUT_DATA_URL = "/branded_web/homepage/get_data";

const isDev = process.env.NODE_ENV !== "production";

const useFetchLayoutData = (props) => {
  const { version } = props;

  const subdomain = getSubdomain(window.location.href);

  const company = isDev ? "testen" : subdomain;

  const { data, isLoading, isValidating, error } = useSWR(
    version
      ? `${LAYOUT_DATA_URL}?company_name=${company}&type=${version}`
      : null
  );

  return { data, isLoading, isValidating, error };
};

export default useFetchLayoutData;
