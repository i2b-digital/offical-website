import useSWR from "swr";
import { getSubdomain } from "@/util/getSubdomain";

const VERSION_URL = "/branded_web/homepage/version";

const useVersion = () => {
  const isDev = process.env.NODE_ENV !== "production";

  const subdomain = getSubdomain(window.location.href);

  const company = isDev ? "testen" : subdomain;

  const {
    data: version,
    error,
    isLoading,
    isValidating,
  } = useSWR(`${VERSION_URL}?company_name=${company}`);

  return { version, error, isLoading, isValidating };
};

export default useVersion;
