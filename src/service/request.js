import axios from "axios";

const instance = axios.create({
  baseURL: "https://wh.i2b.digital/ipo-service-dev",
  timeout: 15000,
});

instance.defaults.headers.post["Content-Type"] = "application/json";

// instance.defaults.withCredentials = false;

// Add a request interceptor
instance.interceptors.request.use(
  function (config) {
    return config;
  },

  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  async function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);

export default instance;
