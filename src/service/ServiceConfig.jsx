import { SWRConfig } from "swr";
import request from "@/service/request";

const fetcher = (url, params) =>
  request.get(url, params).then((res) => res.data);

export const ServiceConfig = (props) => {
  const globalErrorHandler = (error, key) => {
    console.log("globalErrorHandler: ", error);
  };

  return (
    <SWRConfig
      value={{
        errorRetryCount: 3,
        errorRetryInterval: 20000,
        fetcher: fetcher,
        onError: globalErrorHandler,
      }}
    >
      {props.children}
    </SWRConfig>
  );
};
