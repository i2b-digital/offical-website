import { useEffect, useState } from "react";
import { useLoaderData } from "react-router-dom";
import Loading from "@/components/Loading";
import Header from "@/components/header";
import Banner from "@/components/hanner";
import IntroductionSection from "@/components/introduction-section";
import ImageFeatures from "@/components/image-features";
import YoutubeIndroductionSection from "@/components/youtube-introduction-section";
import BackgroundBanner from "@/components/background-banner";
import Footer from "@/components/footer";
import ContactSection from "@/components/contact";

export default function () {
  const { data, error } = useLoaderData();

  const [header, setHeader] = useState({});
  const [footer, setFooter] = useState({});
  const [banner, setBanner] = useState({});
  const [introduction, setIntroduction] = useState({});
  const [backgroundBanner, setBackgroundBanner] = useState({});
  const [features, setFeatures] = useState([]);
  const [youtube, setYouTube] = useState({});
  const [contact, setContact] = useState({});

  useEffect(() => {
    setHeader({
      logo: data?.data?.image_logo,
      navs: [
        data?.data.navigation_1,
        data?.data.navigation_2,
        data?.data.navigation_3,
      ],
    });

    setFooter({
      ig: data?.data.instagram_url,
      fb: data?.data.facebook_url,
    });

    setBanner({
      title: data?.data.banner_desc,
      image: data?.data.image_banner,
      video: data?.data.video_banner,
    });

    setIntroduction({
      title: data?.data.intro_title,
      description: data?.data.intro_desc,
      imageUrl: data?.data.intro_image,
    });

    setBackgroundBanner({
      imageUrl: data?.data.image_content,
    });

    setFeatures([
      {
        url: data?.data.background_1_url,
        icon: data?.data.image_bg_1,
        title: data?.data.background_title_1,
        description: data?.data.background_desc_1,
      },
      {
        url: data?.data.background_2_url,
        icon: data?.data.image_bg_2,
        title: data?.data.background_title_2,
        description: data?.data.background_desc_2,
      },
      {
        url: data?.data.background_3_url,
        icon: data?.data.image_bg_3,
        title: data?.data.background_title_3,
        description: data?.data.background_desc_3,
      },
    ]);

    setYouTube({
      image: data?.data.youtube_image,
      link1: data?.data.youtube_link1_url,
      link2: data?.data.youtube_link2_url,
      title: data?.data.youtube_title,
    });

    setContact({
      image: data?.data.file_contact_image,
      phone: data?.data.phone,
      fax: data?.data.fax,
      email: data?.data.email,
      address: data?.data.address,
    });
  }, [data]);

  if (error) {
    return null;
  }

  return (
    <div>
      <Header logo={header?.logo} navs={header?.navs} />

      <Banner
        title={banner?.title}
        videoUrl={banner?.video}
        imageUrl={banner?.image}
        className="h-screen md:h-screen"
      />

      <IntroductionSection
        index="0"
        title={introduction?.title}
        description={introduction?.description}
        imageUrl={introduction?.imageUrl}
        bgClassName="bg-[#F8F8F8] py-10 md:py-16"
      />

      <ImageFeatures
        data={features}
        title={header?.navs ? header.navs[1] : ""}
      />

      <YoutubeIndroductionSection
        data={[youtube?.link1, youtube?.link2]}
        image={youtube?.image}
        title={header?.navs ? header.navs[2] : ""}
      />

      <BackgroundBanner
        className="my-10"
        imageUrl={backgroundBanner?.imageUrl}
      />

      <ContactSection
        image={contact?.image}
        phone={contact?.phone}
        fax={contact?.fax}
        email={contact?.email}
        address={contact?.address}
      />

      <Footer ig={footer.ig} fb={footer.fb} />
    </div>
  );
}
