import { useState, useEffect } from "react";
import Header from "@/components/header";
import Footer from "@/components/footer";
import Loading from "@/components/Loading";
import { useLoaderData } from "react-router-dom";
import Banner from "@/components/hanner";
import TextBanner from "@/components/text-banner";
import IntroductionSection from "@/components/introduction-section";
import BackgroundBanner from "@/components/background-banner";
import Features from "@/components/features";

export default function () {
  const { data, isLoading, error } = useLoaderData();

  const [header, setHeader] = useState({});
  const [footer, setFooter] = useState({});
  const [banner, setBanner] = useState({});
  const [textBanner, setTextBanner] = useState({});
  const [introduction, setIntroduction] = useState({});
  const [backgroundBanner, setBackgroundBanner] = useState({});
  const [introductionSecond, setIntroductionSecond] = useState({});
  const [features, setFeatures] = useState({});
  const [recommend, setRecommend] = useState({});

  useEffect(() => {
    setHeader({
      logo: data?.data?.image_logo,
      navs: [
        data?.data.navigation_1,
        data?.data.navigation_2,
        data?.data.navigation_3,
      ],
    });

    setFooter({
      ig: data?.data.instagram_url,
      fb: data?.data.facebook_url,
    });

    setBanner({
      title: data?.data.banner_desc,
      image: data?.data.image_banner,
      video: data?.data.video_banner,
    });

    setTextBanner({
      title: data?.data.slogan_title,
      description: data?.data.slogan_desc,
    });

    setIntroduction({
      title: data?.data.intro_title,
      description: data?.data.intro_desc,
      imageUrl: data?.data.intro_image,
    });

    setBackgroundBanner({
      imageUrl: data?.data.image_content,
      title: data?.data.content_title,
    });

    setIntroductionSecond({
      imageUrl: data?.data.intro2_image,
      title: data?.data.intro2_title,
      description: data?.data.intro2_desc,
    });

    setFeatures([
      {
        icon: data?.data.image_bg_1,
        title: data?.data.background_title_1,
        description: data?.data.background_desc_1,
      },
      {
        icon: data?.data.image_bg_2,
        title: data?.data.background_title_2,
        description: data?.data.background_desc_2,
      },
      {
        icon: data?.data.image_bg_3,
        title: data?.data.background_title_3,
        description: data?.data.background_desc_3,
      },
    ]);

    setRecommend({
      url: data?.data.website_url,
      title: data?.data.footer_desc,
      imageUrl: data?.data.image_footer,
    });
  }, [data]);

  if (error) {
  }

  if (isLoading) {
    return <Loading />;
  }

  return (
    <>
      <Header logo={header?.logo} navs={header?.navs} />

      <Banner
        title={banner?.title}
        videoUrl={banner?.video}
        imageUrl={banner?.image}
      />

      <TextBanner
        title={textBanner?.title}
        description={textBanner?.description}
      />

      <IntroductionSection
        index="0"
        title={introduction?.title}
        description={introduction?.description}
        imageUrl={introduction?.imageUrl}
      />

      <BackgroundBanner
        className="my-20 md:my-40"
        title={backgroundBanner?.title}
        imageUrl={backgroundBanner?.imageUrl}
      />

      <IntroductionSection
        index="1"
        title={introductionSecond?.title}
        description={introductionSecond?.description}
        imageUrl={introductionSecond?.imageUrl}
        isReverse
      />

      <Features data={features} title={header?.navs ? header.navs[2] : ""} />

      <BackgroundBanner
        url={recommend?.url}
        title={recommend?.title}
        imageUrl={recommend?.imageUrl}
      />

      <Footer ig={footer.ig} fb={footer.fb} />
    </>
  );
}
