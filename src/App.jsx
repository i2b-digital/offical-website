import { createBrowserRouter, RouterProvider } from "react-router-dom";
import useSWR from "swr";
import { useEffect, useState } from "react";
import NotFound from "./Page/not-found";
import LayoutOne from "./templates/layout-1";
import LayoutTwo from "./templates/layout-2";

import { getSubdomain } from "@/util/getSubdomain";
import Loading from "./components/Loading";
import useVersion from "./hook/useVersion";
import useFetchLayoutData from "./hook/useFetchLayoutData";

const isDev = process.env.NODE_ENV !== "production";

const App = () => {
  const { version, isLoading } = useVersion();

  const [element, setElement] = useState(null);

  useEffect(() => {
    if (version?.data?.type) {
      switch (version.data.type) {
        case "1":
          setElement(<LayoutOne />);
          break;
        case "2":
          setElement(<LayoutTwo />);
          break;
      }
    }
  }, [version]);

  const router = createBrowserRouter([
    {
      path: "/",
      element: element,
      loader: () => useFetchLayoutData({ version: version?.data?.type }),
    },
    {
      path: "*",
      element: <NotFound />,
    },
  ]);

  if (isLoading) {
    return <Loading />;
  }

  return <RouterProvider router={router} />;
};

export default App;
